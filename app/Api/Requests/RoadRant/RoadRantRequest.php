<?php


namespace App\Api\V7\Requests\RoadRant;


use App\Models\RoadRant\RoadRant;
use Illuminate\Foundation\Http\FormRequest;

class RoadRantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['cat_id'] = 'required|integer|not_in:0';
        $rules['content'] = 'required|max:2000';
        $rules['image'] = 'required_without:video|array|max:1';
        $rules['image.*'] = 'file|mimes:'.implode(',',RoadRant::IMAGE_ALLOW_EXT).'|max:10240';
        $rules['video'] = 'required_without:image|array|max:1';
        $rules['video.*'] = 'file|mimes:'.implode(',',RoadRant::VIDEO_ALLOW_EXT).'|max:51200';
//        $rules['mention'] = 'sometimes|array';
//        $rules['mention.*'] = 'integer';
//        $rules['location_name'] = 'sometimes|string';
//        $rules['formatted_address'] = 'sometimes|string';
//        $rules['place_id'] = 'sometimes|string';
        return $rules;
    }
}
