<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanRepayment extends Model
{
    use SoftDeletes;

    protected $table='aspire_loan_repayment';
    protected $primaryKey='lr_id';
    protected $fillable=[
        'lr_loan_id',
        'lr_amount',
    ];
}
