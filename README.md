# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

create db with name : aspire

rename .env.examlpe to : .env

add bellow details in .env
	DB_DATABASE=aspire
	DB_USERNAME=root
	DB_PASSWORD=root
	API_PREFIX=api
	API_SUBTYPE=app
	API_VERSION=v1

composer install

php artisan migrate

php artisan db:seed

php artisan passport:install

php artisan key:generate

php artisan config:cache

php artisan serve 

### Default Login Credential

default user credential is for login: 
email : dipen@yopmail.com
password : 12345678


### API LINK

1. For login 
	api : http://127.0.0.1:8000/api/V1/auth/login
	parameter : email:dipen@yopmail.com
					  password:12345678

2. For create loan for login user
	api : http://127.0.0.1:8000/api/V1/loan/create
	parameter : loan_amount:10
					  loan_term:1
	authorization : OAuth2.0 (insert token from login api response)

3. For update loan status
	api : http://127.0.0.1:8000/api/V1/loan/updateStatus
	parameter : loan_id:1
					  loan_status:0
	authorization : OAuth2.0 (insert token from login api response)
	
4. For loan repayment 
	api : http://127.0.0.1:8000/api/V1/loan-repayment/create
	parameter : loan_id:1
					  loan_repayment_amount:5
	authorization : OAuth2.0 (insert token from login api response)
