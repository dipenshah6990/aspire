<?php
$api->group(['prefix' => 'auth'], function ($api) {
    $api->post('login', ['as' => 'login','uses' => 'Auth\LoginController@login']);
});
$api->group(['middleware' => 'auth:api'], function ($api) {
    $api->group(['prefix' => 'loan'], function ($api) {
        $api->post('/create', 'Loan\LoanController@create');
        $api->post('/updateStatus', 'Loan\LoanController@updateStatus');
    });
    $api->group(['prefix' => 'loan-repayment'], function ($api) {
        $api->post('/create', 'Loan\LoanRepaymentController@create');
    });
});
