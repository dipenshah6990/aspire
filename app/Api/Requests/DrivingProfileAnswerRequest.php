<?php

namespace App\Api\V7\Requests;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class DrivingProfileAnswerRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'dpq_id' => 'required|exists:rh_driving_profile_questions,dpq_id,deleted_at,NULL',
            'dpq_type' => 'required|numeric',
            'left_marker_answer' => 'required|numeric',
            'right_marker_answer' => 'required|numeric',
            'dpq_answer' => 'required_if:dpq_type,3'
        ];
    }

    public function messages() {
        return [
            'dpq_id.required' => 'Missing required Parameter: dpq_id',
            'dpq_id.exists' => 'Invalid Parameter: dpq_id',
            'dpq_type' => 'Missing required Parameter: dpq_type',
            'dpq_type.numeric' => 'Invalid Parameter: dpq_type',
            'left_marker_answer.required' => 'Missing required Parameter: left_marker_answer',
            'left_marker_answer.numeric' => 'Invalid Parameter: left_marker_answer',
            'right_marker_answer.required' => 'Missing required Parameter: right_marker_answer',
            'right_marker_answer.numeric' => 'Invalid Parameter: right_marker_answer',
            'dpq_answer.required' => 'Missing required Parameter: dpq_answer',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
