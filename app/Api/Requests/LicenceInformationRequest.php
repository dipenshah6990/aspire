<?php

namespace App\Api\V7\Requests;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class LicenceInformationRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'user_id' => 'required',
            'up_driving_licence_no' => 'required',
            'licence_issuing_agency' => 'required',
//            'licence_start_date' => 'required|date',
//            'licence_expiry_date' => 'required|date',
//            'licence_restriction_ids' => 'required',
//            'licence_category_ids' => 'required',
            'match_percent' => 'required',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
