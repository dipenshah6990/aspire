<?php

namespace App\Api\V7\Requests;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class RegisterRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required',
//                'title' => 'required',
//                'dob' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'user_phone' => 'required|digits_between:9,16',
            'user_phone_country_code' => 'required|digits_between:1,4',
            'device_type' => 'required',
            'device_token' => 'required',
            'corporate_token' => 'max:24',
        ];
    }
    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
