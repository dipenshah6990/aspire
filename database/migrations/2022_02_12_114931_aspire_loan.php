<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AspireLoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspire_loan', function (Blueprint $table) {
            $table->bigIncrements('l_id');
            $table->bigInteger('user_id');
            $table->double('l_amount',10,2);
            $table->double('l_due_amount',10,2);
            $table->integer('l_term');
            $table->tinyInteger('l_status')->comment('0-decline,1-approved');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspire_loan');
    }
}
