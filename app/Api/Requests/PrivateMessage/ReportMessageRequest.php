<?php


namespace App\Api\V7\Requests\PrivateMessage;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReportMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['message_id'] = 'required|integer|exists:rh_private_messages,id';
        return $rules;
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => new \stdClass()];
        throw new HttpResponseException(response($responseArr,422));
    }
}
