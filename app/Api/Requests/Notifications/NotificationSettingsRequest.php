<?php

namespace App\Api\V7\Requests\Notifications;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class NotificationSettingsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'uns_limit_per_day' => 'required',
            'uns_start_time' => 'required',
            'uns_end_time' => 'required',
            'uns_sound' => 'required',
            'uns_default_exam_mode' => 'nullable|sometimes|integer'
        ];
    }

    public function messages() {
        return [
            'uns_limit_per_day.required' => 'Missing required Parameter: uns_limit_per_day',
            'uns_start_time.required'=>'Missing required Parameter: uns_start_time',
            'uns_end_time.required'=>'Missing required Parameter: uns_end_time',
            'uns_sound.required' => 'Missing required Parameter: uns_sound',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
