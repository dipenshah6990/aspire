<?php


namespace App\Api\V7\Requests\RoadTrip;


use Illuminate\Foundation\Http\FormRequest;

class ShareRoadTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['road_trip_id'] = 'sometimes|required|integer';
        $rules['receiver_id'] = 'sometimes|required|array';
        $rules['lat'] = 'sometimes|required';
        $rules['long'] = 'sometimes|required';
        return $rules;
    }
}
