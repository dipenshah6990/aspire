<?php


namespace App\Api\V7\Requests\RoadTrip;


use App\Models\RoadTrip\RoadTrip;
use Illuminate\Foundation\Http\FormRequest;

class UserRoadTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['id'] = 'sometimes|integer';
        $rules['source'] = 'required_without:id|json';
        $rules['destination'] = 'required_without:id|json';
        $rules['trip_title'] = 'required_without:id';
        $rules['time_duration'] = 'required_without:id';
        $rules['distance'] = 'required_without:id';
        $rules['static_route_image'] = 'required_without:id';
        //$rules['sea_level_height'] = 'required_without:id';
        $rules['status'] = 'required|integer';
       // $rules['trip_route'] = 'sometimes|required';
        return $rules;
    }
}
