<?php

namespace App\Api\V7\Requests\Post;


use Dingo\Api\Http\FormRequest;

class PostStoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['content'] = 'sometimes|max:280';
        return $rules;
    }
}
