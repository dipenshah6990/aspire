<?php


namespace App\Api\V7\Requests\RoadRant;


use App\Models\RoadRant\RoadRant;
use App\Models\RoadRant\RoadRantComment;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RoadRantUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $roadRant = RoadRant::where('user_id', $this->user()->id)->where('id', $this->id)->first();
        return $roadRant != NULL;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['id'] = 'required|integer|exists:rh_road_rant,id';
//        $rules['cat_id'] = 'sometimes|integer';
//        $rules['content'] = 'sometimes|max:2000';
//        $rules['postcode'] = 'sometimes|string';
//        $rules['city'] = 'sometimes|string';
//        $rules['county'] = 'sometimes|string';
//        $rules['mention'] = 'sometimes|array';
//        $rules['mention.*'] = 'integer';
        return $rules;
    }

    public function failedValidation(Validator $validator)
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr, 422));
    }
}
