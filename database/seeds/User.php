<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = $this->records();
        foreach ($records as $record) {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => $record['name'],
                'email' => $record['email'],
                'password' => $record['password'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
    protected function records() {
        return [
            ['name' => 'Dipen Shah',
             'email' => 'dipen@yopmail.com',
             'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
            ],
        ];
    }
}
