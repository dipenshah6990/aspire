<?php

namespace App\Api\Controllers\Loan;

use \App\Api\Controllers\BaseApiController;
use App\Api\Requests\Auth\LoginRequest;
use App\Loan;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;

class LoanController extends BaseApiController {

    public function index(Request $request){
        dd($request->all());
    }

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define insert new loan record for login user
     */
    public function create(Request $request){
        /* validate if request has valid data or not */
        try{
            $request->validate([
                "loan_amount" => "required|numeric|gt:0",
                "loan_term" => "required|numeric|gt:0"
            ]);
        }catch (\Exception $e){
            $error_message='';
            foreach ($e->errors() as $error){
                if($error_message=='')
                    $error_message=(implode(',',$error));
                else
                    $error_message.=','.(implode(',',$error));
            }
            return $this->ApiResponseError([], $error_message, 200);
        }

        /* create seperate entry for all loan for given user with given amount and term */
        $loanData= Loan::create([
            "user_id"=>auth('api')->id(),
            "l_amount"=>$request->loan_amount,
            "l_due_amount"=>$request->loan_amount,
            "l_term"=>$request->loan_term,
            "l_status"=>config('constants.ACCEPTED'),
        ]);

        /* return new created loan response */
        return $this->ApiResponseSuccess($loanData, 'Loan added successfully', 200);
    }

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define update loan status depend on given loan id
     */
    public function updateStatus(Request $request){

        /* validate if request has valid data or not */
        try{
            $request->validate([
                "loan_id" => "required|numeric|gt:0",
                "loan_status" => "required|numeric|in:0,1"
            ]);
        }catch (\Exception $e){
            $error_message='';
            foreach ($e->errors() as $error){
                if($error_message=='')
                    $error_message=(implode(',',$error));
                else
                    $error_message.=','.(implode(',',$error));
            }
            return $this->ApiResponseError([], $error_message, 200);
        }
        /* create loan object */
        $query = Loan::where('l_id',$request->loan_id);

        /* check if query has data or not , if yes then we update status else error */
        if($query->count() > 0) {

            /* update loan status */
            $query->update(['l_status' => ($request->loan_status == config('constants.ACCEPTED')) ? config('constants.ACCEPTED') : config('constants.DECLINE')]);
            $loanData = $query->get();

            /* return response */
            return $this->ApiResponseSuccess($loanData, 'Loan added successfully', 200);

        }else{

            /* return response */
            return $this->ApiResponseSuccess([], 'enter valid loan id', 200);
        }
    }
}
