<?php


namespace App\Api\V7\Requests\RoadTrip;


use Illuminate\Foundation\Http\FormRequest;

class UserLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['lat'] = 'required';
        $rules['long'] = 'required';
        return $rules;
    }
}
