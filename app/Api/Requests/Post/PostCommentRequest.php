<?php


namespace App\Api\V7\Requests\Post;


use App\Models\SocialFeed\PostComment;
use Dingo\Api\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PostCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        //$rules['parent_id'] = 'sometimes|integer|exists:rh_post_comments,id';
        $rules['post_id'] = 'required|integer|exists:rh_feed_posts,id';
        $rules['comment'] = 'required|string';
        return $rules;
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 422));
    }
}
