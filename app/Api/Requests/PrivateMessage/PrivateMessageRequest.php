<?php


namespace App\Api\V7\Requests\PrivateMessage;


use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PrivateMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['conversation_key'] = 'required|string';
        $rules['receiver_id'] = 'required|integer|exists:'.with(new User())->getTable().',id';
        $rules['body'] = 'sometimes|max:2000';
        $rules['media'] = 'sometimes';
        $rules['type'] = 'sometimes|integer';
        return $rules;
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => new \stdClass()];
        throw new HttpResponseException(response($responseArr,422));
    }
}
