<?php

namespace App\Api\Controllers\Loan;

use \App\Api\Controllers\BaseApiController;
use App\Api\Requests\Auth\LoginRequest;
use App\Loan;
use App\LoanRepayment;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;

class LoanRepaymentController extends BaseApiController {

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define insert new loan repayment record for specific loan
     */
    public function create(Request $request){
        /* validate if request has valid data or not */
        try{
            $request->validate([
                "loan_amount" => "required|numeric|gt:0",
                "loan_id" => "required|numeric|gt:0"
            ]);
        }catch (\Exception $e){
            $error_message='';
            foreach ($e->errors() as $error){
                if($error_message=='')
                    $error_message=(implode(',',$error));
                else
                    $error_message.=','.(implode(',',$error));
            }
            return $this->ApiResponseError([], $error_message, 200);
        }

        /* check if given loan id status is active or not , if not then we not able to accept repayment and we give them error */
        $loan = Loan::where(['l_id'=>$request->loan_id,'l_status'=>config('constants.ACCEPTED')])->first();
        if(!isset($loan)){
            return $this->ApiResponseError([], 'Please select loan which is accepted.', 200);
        }else{
            /* check if given repayment load amount is not higher than remain due amount */
            if($loan->l_due_amount <= $request->loan_repayment_amount){
                return $this->ApiResponseError([], 'Please insert valid repay amount.', 200);
            }
        }

        /* create seperate entry for all repayment */
        $loanRepaymentData= LoanRepayment::create([
            "loan_id"=>$request->loan_id,
            "l_amount"=>$request->loan_repayment_amount,
        ]);

        /* update loan due amount  */
        $loanData = Loan::where(['l_id'=>$request->loan_id])->first();
        $loanData->l_due_amount = $loanData->l_due_amount - $request->loan_repayment_amount;
        $loanData->save();

        /* return response */
        return $this->ApiResponseSuccess($loanData, 'Loan added successfully', 200);
    }

}
