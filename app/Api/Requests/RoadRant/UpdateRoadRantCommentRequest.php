<?php


namespace App\Api\V7\Requests\RoadRant;


use App\Models\RoadRant\RoadRantComment;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateRoadRantCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $comment = RoadRantComment::where('user_id', $this->user()->id)->where('id', $this->id)->first();
        return $comment != NULL;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['id'] = 'required|integer|exists:rh_road_rant_comments,id';
        $rules['parent_id'] = 'sometimes|integer|exists:rh_road_rant_comments,parent_id';
        $rules['comment'] = 'sometimes|string';
        return $rules;
    }

    public function failedValidation(Validator $validator)
    {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr, 422));
    }
}
