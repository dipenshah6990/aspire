<?php

namespace App\Api\V7\Requests\CourseQuiz;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class QuizModeQuestionRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'course_id' => 'required|exists:rh_courses,course_id',
            'uc_id' => 'sometimes|nullable|exists:rh_user_course,uc_id',
            'notification_id' => 'required'
        ];
    }

    public function messages() {
        return [
            'course_id.required' => 'Missing required Parameter: course_id',
            'course_id.exists' => 'Invalid Parameter: course_id',
            'uc_id.exists' => 'Invalid Parameter: uc_id',
            'notification_id' => 'Missing required Parameter: notification_id'
            //'cq_status.required' => 'Missing required Parameter: cq_status',
            //'cq_status.in' => 'Quiz mode can either be enabled or disabled',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
