<?php

namespace App\Api\V7\Requests\CourseQuiz;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CourseQuizModeRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'cq_course_id' => 'required|exists:rh_courses,course_id',
            'cq_status' => 'required|in:0,1',
        ];
    }

    public function messages() {
        return [
            'cq_course_id.required' => 'Missing required Parameter: cq_course_id',
            'cq_course_id.exists' => 'Invalid Parameter: cq_course_id',
            'cq_status.required' => 'Missing required Parameter: cq_status',
            'cq_status.in' => 'Quiz mode can either be enabled or disabled',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
