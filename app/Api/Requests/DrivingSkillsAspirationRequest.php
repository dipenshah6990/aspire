<?php

namespace App\Api\V7\Requests;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class DrivingSkillsAspirationRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
//            'user_id' => 'required',
//            'driving_aspiration_ids' => 'required',
//            'driving_skills_ids' => 'required',
            'step' => 'required',
            'answer' => 'required|numeric',
        ];
    }

    public function messages() {
        return [
            'step.required' => 'Missing required Parameter: step',
            'answer.numeric' => 'Invalid Parameter: answer',
            'answer.required' => 'Missing required Parameter: answer',
            'answer.not_in' => 'Invalid Parameter: answer',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
