<?php


namespace App\Api\V7\Requests\RoadRant;


use App\Models\RoadRant\RoadRant;
use Illuminate\Foundation\Http\FormRequest;

class RoadRantListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['postcode'] = 'sometimes|string';
        $rules['city'] = 'sometimes|string';
        $rules['county'] = 'sometimes|string';
        $rules['start_date'] = 'sometimes|string';
        $rules['end_date'] = 'sometimes|string';
        $rules['category'] = 'sometimes|string';
        return $rules;
    }
}
