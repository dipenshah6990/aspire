<?php

namespace App\Api\V7\Requests;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class VehicleInformationRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'user_id' => 'required',
//            'vehicle_manufacturer_id' => 'required',
//            'vehicle_model_id' => 'required',
//            'vehicle_colour_id' => 'required',
//            'vehicle_type_id' => 'required',
            'vehicle_manufacturer_id' => 'required',
            //'vehicle_insurer_id' => 'required',
//            'vehicle_purchased_date' => 'required',
//            'vehicle_policy_number' => 'required',
//            'vehicle_policy_start_date' => 'required|date',
//            'vehicle_policy_end_date' => 'required|date',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
