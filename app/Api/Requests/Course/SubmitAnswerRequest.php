<?php

namespace App\Api\V7\Requests\Course;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class SubmitAnswerRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'user_course_id' => 'required|exists:rh_user_course,uc_id',
            'course_id' => 'required|exists:rh_courses,course_id',
            'question_id' => 'required|exists:rh_questions,q_id',
            'ucq_id' => 'required|exists:rh_user_course_questions,ucq_id',
            //'submitted_answers'=>'required',
            'answerWithoutSubmit' => 'required',
            'isLastQuestion' => 'required',
            'timeTaken' => 'required',
            'question_time_taken' => 'required',
            'time_limit' => 'required',
        ];
    }

    public function messages() {
        return [
            'user_course_id.required' => 'Missing required Parameter: user_course_id',
            'user_course_id.exists' => 'Invalid Parameter: user_course_id',

            'course_id.required' => 'Missing required Parameter: course_id',
            'course_id.exists' => 'Invalid Parameter: course_id',

            'question_id.required' => 'Missing required Parameter: question_id',
            'question_id.exists' => 'Invalid Parameter: question_id',

            'ucq_id.required' => 'Missing required Parameter: ucq_id',
            'ucq_id.exists' => 'Invalid Parameter: ucq_id',

            'time_limit.required' => 'Missing required Parameter: time_limit',
            'question_time_taken.required' => 'Missing required Parameter: question_time_taken',
            'timeTaken.required' => 'Missing required Parameter: timeTaken',
            'answerWithoutSubmit.required' => 'Missing required Parameter: answerWithoutSubmit',
            'isLastQuestion.required' => 'Missing required Parameter: isLastQuestion',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
