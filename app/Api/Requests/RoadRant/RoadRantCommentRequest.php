<?php


namespace App\Api\V7\Requests\RoadRant;


use Illuminate\Foundation\Http\FormRequest;

class RoadRantCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['road_rant_id'] = 'required|integer|exists:rh_road_rant,id';
        $rules['comment'] = 'required|string';
        return $rules;
    }
}
