<?php


namespace App\Api\V7\Requests\SupportMessage;


use App\Models\Admin\CMS\HelpTopicsCategories;
use App\Models\SupportMessages\SupportMessage;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SupportMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['id'] = 'sometimes|integer|exists:'.with(new SupportMessage())->getTable().',id';
        $rules['sender_id'] = 'sometimes|integer';
        $rules['recipient_id'] = 'sometimes|integer';
        $rules['body'] = 'sometimes|max:2000';
        $rules['media'] = 'sometimes|max:1';
        $rules['module_id'] = 'sometimes|integer|exists:'.with(new HelpTopicsCategories())->getTable().',ht_category_id';
        $rules['module_title'] = 'sometimes|string';
        return $rules;
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => new \stdClass()];
        throw new HttpResponseException(response($responseArr,422));
    }
}
