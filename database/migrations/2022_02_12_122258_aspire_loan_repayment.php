<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AspireLoanRepayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aspire_loan_repayment', function (Blueprint $table) {
            $table->bigIncrements('lr_id');
            $table->biginteger('lr_loan_id')->comment('loan table primary key');
            $table->double('lr_amount',10,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aspire_loan_repayment');
    }
}
