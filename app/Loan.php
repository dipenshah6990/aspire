<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;

    protected $table='aspire_loan';
    protected $primaryKey='l_id';
    protected $fillable=[
        'user_id',
        'l_amount',
        'l_due_amount',
        'l_term',
        'l_status'
    ];
}
