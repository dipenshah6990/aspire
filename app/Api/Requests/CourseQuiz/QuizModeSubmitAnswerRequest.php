<?php

namespace App\Api\V7\Requests\CourseQuiz;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class QuizModeSubmitAnswerRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'ucq_id' => 'required|exists:rh_user_course_questions,ucq_id',
            'answerWithoutSubmit' => 'required',
            'time_limit' => 'required',
            'question_time_taken' => 'required',
        ];
    }

    public function messages() {
        return [
            'ucq_id.required' => 'Missing required Parameter: ucq_id',
            'ucq_id.exists' => 'Invalid Parameter: ucq_id',
            'answerWithoutSubmit.required' => 'Missing required Parameter: answerWithoutSubmit',
            'time_limit.required' => 'Missing required Parameter: time_limit',
            'question_time_taken.required' => 'Missing required Parameter: question_time_taken',
        ];
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
