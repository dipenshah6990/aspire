<?php


namespace App\Api\V7\Requests\Post;


use Illuminate\Foundation\Http\FormRequest;

class StoryReactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['reaction'] = 'required|integer';
        $rules['media_id'] = 'required|integer';
        return $rules;
    }
}
