<?php


namespace App\Api\V7\Requests\Post;


use App\Models\SocialFeed\Post;
use Illuminate\Contracts\Validation\Validator;
use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PostFeedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        if ($this->has('id')) {
            $post = Post::where('user_id', $this->user()->id)->where('id',$this->id)->first();
            return $post != NULL;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [];
        $rules['id'] = 'sometimes|integer|exists:'.with(new Post())->getTable().',id';
        $rules['content'] = 'sometimes|max:2000';
        $rules['image'] = 'nullable|sometimes|array|max:5';
        $rules['image.*'] = 'file|mimes:'.implode(',',Post::IMAGE_ALLOW_EXT).'|max:10240';
        $rules['video'] = 'nullable|sometimes|array|max:1';
        $rules['video.*'] = 'file|mimes:'.implode(',',Post::VIDEO_ALLOW_EXT).'|max:51200';
        $rules['external_video_link'] = 'sometimes|array';
        $rules['course_id'] = 'sometimes|integer';
        $rules['score'] = 'sometimes';
        $rules['percentage'] = 'sometimes';
        $rules['pass_level'] = 'sometimes';
        return $rules;
    }

    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 422, 'success' => false, 'message' => $validator->errors()->first(), 'data' => new \stdClass()];
        throw new HttpResponseException(response($responseArr,422));
    }
}
